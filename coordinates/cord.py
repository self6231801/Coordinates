from fastapi import FastAPI
import uvicorn
from pyzipcode import ZipCodeDatabase
from geopy.geocoders import Nominatim
from pydantic import BaseModel

app = FastAPI()

class LocationInput(BaseModel):
    lat: str
    lng: str

def get_zip_codes(latitude, longitude):
    geolocator = Nominatim(user_agent="zip_code_locator")
    location = geolocator.reverse((latitude, longitude), exactly_one=True, timeout=None)

    if location and 'address' in location.raw:
        address = location.raw['address']
        if 'postcode' in address:
            return address['postcode']
    
    return None

@app.post("/get_zipcodes")
async def get_zipcodes(location_input: LocationInput):
    latitude = float(location_input.lat)
    longitude = float(location_input.lng)

    zip_code = get_zip_codes(latitude, longitude)
    zcdb = ZipCodeDatabase()
    in_radius = [z.zip for z in zcdb.get_zipcodes_around_radius(zip_code, 10)]
    print(in_radius)
   
    return {"zipcodes": in_radius}

if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8003)
